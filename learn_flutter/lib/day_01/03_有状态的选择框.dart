import 'package:flutter/material.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: HomePage()
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text("第一个Flutter程序"),
          backgroundColor: Colors.brown,
        ),
        body: HomeContentbody()
    );
  }
}

class HomeContentbody extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return HomeContentbodyState();
  }
}

class HomeContentbodyState extends State<HomeContentbody> {
  var flag = true;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      color: Colors.green,
      child: Center(
          child: Row(
            children: [
              Checkbox(
                value: flag,
                onChanged: (value) {
                  setState(() {
                    flag = value;
                  });
                  print("点击：$value");
                },
              ),
              Text(
                "文本二",
                style: TextStyle(
                    fontSize: 20,
                    color: Colors.brown
                ),
              )
            ],
            mainAxisAlignment: MainAxisAlignment.center,
          )
      ),
    );
  }
}