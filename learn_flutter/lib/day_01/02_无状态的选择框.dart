import 'package:flutter/material.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: HomePage()
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text("第一个Flutter程序"),
          backgroundColor: Colors.brown,
        ),
        body: HomeContentbody()
    );
  }
}

class HomeContentbody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      color: Colors.green,
      child: Center(
        child: Text(
          "hello world",
          style: TextStyle(
              color: Colors.brown,
              fontSize: 50
          ),
        ),
      ),
    );
  }
}