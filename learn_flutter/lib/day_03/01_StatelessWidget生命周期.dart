import 'dart:ui';

import 'package:flutter/material.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: JKHomePage(),
    );
  }
}

class JKHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "商品列表",
          style: TextStyle(
              fontSize: 20,
              color: Colors.yellow
          ),
        ),
        backgroundColor: Colors.purple,
      ),
      body: JKHomeContent("你好，王大帅"),
    );
  }
}

class JKHomeContent extends StatelessWidget {
  final String message;

  JKHomeContent(this.message) {
    print("构造函数被调用");
  }

  @override
  Widget build(BuildContext context) {
    print("调用 build 方法");
    return Text(message);
  }
}


