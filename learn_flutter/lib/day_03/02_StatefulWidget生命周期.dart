import 'dart:ui';

import 'package:flutter/material.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: JKHomePage(),
    );
  }
}


class JKHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "商品列表",
          style: TextStyle(
              fontSize: 20,
              color: Colors.yellow
          ),
        ),
        backgroundColor: Colors.purple,
      ),
      body: JKHomeContent(),
    );
  }
}



class JKHomeContent extends StatefulWidget {
  JKHomeContent() {
    print("1.调用JKHomeContent的constructor");
  }
  @override
  _JKHomeContentState createState() {
    print("2.调用JKHomeContent的createState");
    return _JKHomeContentState();
  }
}

class _JKHomeContentState extends State<JKHomeContent> {

  _JKHomeContentState() {
    print("3.调用_JKHomeContentState的constructor");
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print("4.调用_JKHomeContentState的initState");
  }
  @override
  Widget build(BuildContext context) {
    print("5.调用_JKHomeContentState的build");
    return Container();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    print("6.调用_JKHomeContentState的dispose方法");
  }
}

