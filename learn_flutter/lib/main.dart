import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Flutter Demo",
      debugShowCheckedModeBanner: false,
      home: JKHomePage(),
    );
  }
}

class JKHomePage extends StatefulWidget {
  @override
  _JKHomePageState createState() => _JKHomePageState();
}

class _JKHomePageState extends State<JKHomePage> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print("哈哈");
    getHttp();
  }

  void getHttp() async {
    try {
      var response = await Dio().get('http://rest.apizza.net/mock/2fb26a6d73ae778d0ca37bb239e38397/home/meal');
      print("测试 $response");
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "标题",
            style: TextStyle(
                fontSize: 22,
                color: Colors.yellow
            ),
          ),
        ),
        body: Container()
    );
  }
}