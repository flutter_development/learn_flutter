import 'dart:ui';

import 'package:flutter/material.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: JKHomePage(),
    );
  }
}

class JKHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "小控件",
            style: TextStyle(
                fontSize: 22,
                color: Colors.yellow
            ),
          ),
        ),
        body: JKHomeContent()
    );
  }
}
class JKHomeContent extends StatefulWidget {
  @override
  _JKHomeContentState createState() => _JKHomeContentState();
}

class _JKHomeContentState extends State<JKHomeContent> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: FadeInImage(
              fadeOutDuration: Duration(milliseconds: 1),
              fadeInDuration: Duration(milliseconds: 1),
              placeholder: AssetImage("assets/images/juren.jpeg"),
              image: NetworkImage("https://tva1.sinaimg.cn/large/006y8mN6gy1g7aa03bmfpj3069069mx8.jpg")
          )
      ),
    );
  }
}

