import 'dart:ui';

import 'package:flutter/material.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: JKHomePage(),
    );
  }
}

class JKHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "小控件",
            style: TextStyle(
                fontSize: 22,
                color: Colors.yellow
            ),
          ),
        ),
        body: JKHomeContent()
    );
  }
}
class JKHomeContent extends StatefulWidget {
  @override
  _JKHomeContentState createState() => _JKHomeContentState();
}

class _JKHomeContentState extends State<JKHomeContent> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ElevatedButton(
          child: Text("审核完成"),
          style: ButtonStyle(
            side: MaterialStateProperty.all(BorderSide(width: 1,color: Color(0xffCAD0DB))),//边框
            shape: MaterialStateProperty.all(BeveledRectangleBorder(borderRadius: BorderRadius.circular(8))),//圆角弧度
          ),
          onPressed: () {
            print('我被点击了：棱形');
          },
        ),
        ElevatedButton(
          child: Text("审"),
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(Color(0xffffffff)),                   // 背景颜色
            foregroundColor: MaterialStateProperty.all(Color(0xff5E6573)),                   // 字体颜色
            overlayColor: MaterialStateProperty.all(Color(0xffffffff)),                      // 高亮色
            shadowColor: MaterialStateProperty.all( Color(0xffffffff)),                      // 阴影颜色
            elevation: MaterialStateProperty.all(0),                                         // 阴影值
            textStyle: MaterialStateProperty.all(TextStyle(fontSize: 12)),                   // 字体
            side: MaterialStateProperty.all(BorderSide(width: 1,color: Color(0xffCAD0DB))),  // 边框
            shape: MaterialStateProperty.all(
                CircleBorder(
                    side: BorderSide(
                      //设置 界面效果
                      color: Colors.brown,
                      width: 280.0,
                      style: BorderStyle.none,
                    )
                )
            ),//圆角弧度

          ),
          onPressed: () {},
        ),
        ElevatedButton(
          child: Text("审核完成"),
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(Color(0xffffffff)),                // 背景颜色
            foregroundColor: MaterialStateProperty.all(Color(0xff5E6573)),                // 字体颜色
            overlayColor: MaterialStateProperty.all(Color(0xffffffff)),                   // 高亮色
            shadowColor: MaterialStateProperty.all( Color(0xffffffff)),                   // 阴影颜色
            elevation: MaterialStateProperty.all(0),                                      //阴影值
            textStyle: MaterialStateProperty.all(TextStyle(fontSize: 12)),                //字体
            side: MaterialStateProperty.all(BorderSide(width: 1,color: Color(0xffCAD0DB))),//边框
            shape: MaterialStateProperty.all(
                StadiumBorder(
                    side: BorderSide(
                      //设置 界面效果
                      style: BorderStyle.solid,
                      color: Color(0xffFF7F24),
                    )
                )
            ),//圆角弧度
          ),
          onPressed: () {
            print('我被点击了：圆形');
          },
        ),
        IconButton(
          icon: Icon(Icons.volume_up, size: 48.0,),
          tooltip: '按下操作',
          color: Colors.brown,
          onPressed: () {
            print("按下操作");
          },
        ),
        TextButton(
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(Colors.brown),
            ),
            onPressed: () {
              print("喜欢");
            },
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text("喜欢"),
                Icon(Icons.favorite, color: Colors.red,)
              ],
            )
        )
      ],
    );
  }
}

