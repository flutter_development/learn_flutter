import 'dart:ui';

import 'package:flutter/material.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: JKHomePage(),
    );
  }
}

class JKHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "小控件",
            style: TextStyle(
                fontSize: 22,
                color: Colors.yellow
            ),
          ),
        ),
        body: JKHomeContent()
    );
  }
}
class JKHomeContent extends StatefulWidget {
  @override
  _JKHomeContentState createState() => _JKHomeContentState();
}

class _JKHomeContentState extends State<JKHomeContent> {

  final usernameTextEditController = TextEditingController();
  final passwordTextEditController = TextEditingController();
  FocusNode _commentFocus = FocusNode();
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          TextField(
            focusNode: _commentFocus,
            controller: usernameTextEditController,
            decoration: InputDecoration(
                labelText: "username",
                icon: Icon(Icons.people),
                hintText: "请输入用户名",
                border: OutlineInputBorder(),
                filled: true,
                fillColor: Colors.brown
            ),
            onChanged: (value) {
              print("$value");
            },
            onSubmitted: (value) {
              print("提交：$value");
            },
          ),
          SizedBox(height: 20,),
          TextField(
            controller: passwordTextEditController,
            decoration: InputDecoration(
                labelText: "password",
                icon: Icon(Icons.lock),
                hintText: "请输入密码",
                border: OutlineInputBorder(),
                filled: true,
                fillColor: Colors.brown
            ),
            onChanged: (value) {
              print("$value");
            },
            onSubmitted: (value) {
              print("提交：$value");
            },
          ),
          SizedBox(height: 20,),
          Container(
            width: double.infinity,
            height: 32,
            child: TextButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.brown),
                ),
                onPressed: () {
                  print("名字：${usernameTextEditController.text}");
                  print("密码：${passwordTextEditController.text}");
                  _commentFocus.unfocus();    // 失去焦点
                },
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text("提交")
                  ],
                )
            ),
          )
        ],
      ),
    );
  }
}

