import 'dart:io';

main(List<String> args) {
  print("main start");
  getData().then((value) {
    print(value);
  }).catchError((error) {
    print(error);
  });
  print("main end");
}

Future getData() async {
  var result1 = await getNetWorkData("argument1");
  print(result1);
  var result2 = await getNetWorkData(result1);
  print(result2);
  var result3 = await getNetWorkData(result2);
  print(result3);
  return result3;
}

Future getNetWorkData(String arg) {
  return Future(() {
    sleep(Duration(seconds: 5));
    throw Exception("错误");
    // return "Hello World" + arg;
  });
}
