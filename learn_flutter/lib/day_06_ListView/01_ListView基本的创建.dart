import 'package:flutter/material.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Flutter Demo",
      debugShowCheckedModeBanner: false,
      home: JKHomePage(),
    );
  }
}

class JKHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "标题",
            style: TextStyle(
                fontSize: 22,
                color: Colors.yellow
            ),
          ),
        ),
        body: JKHomeContent()
    );
  }
}

class JKHomeContent extends StatefulWidget {
  @override
  _JKHomeContentState createState() => _JKHomeContentState();
}

class _JKHomeContentState extends State<JKHomeContent> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      // reverse: true,
      children: List.generate(50, (index) {
        return Text("第 $index 行", style: TextStyle(fontSize: 30),);
      }),
    );
  }
}
