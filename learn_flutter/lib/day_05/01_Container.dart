import 'dart:ui';

import 'package:flutter/material.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: JKHomePage(),
    );
  }
}

class JKHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "小控件",
            style: TextStyle(
                fontSize: 22,
                color: Colors.yellow
            ),
          ),
        ),
        body: JKHomeContent()
    );
  }
}
class JKHomeContent extends StatefulWidget {
  @override
  _JKHomeContentState createState() => _JKHomeContentState();
}

class _JKHomeContentState extends State<JKHomeContent> {

  final usernameTextEditController = TextEditingController();
  final passwordTextEditController = TextEditingController();
  FocusNode _commentFocus = FocusNode();
  @override
  Widget build(BuildContext context) {
    return  Center(
      child: Container(
        width: 200,
        height: 200,
        decoration: BoxDecoration(
            border: Border.all(
                width: 5,
                color: Colors.yellow
            ),
            borderRadius: BorderRadius.circular(20),
            image: DecorationImage(
              image: NetworkImage("https://tva1.sinaimg.cn/large/006y8mN6gy1g7aa03bmfpj3069069mx8.jpg"),
            ),
            boxShadow: [
              BoxShadow(color: Colors.orange, offset: Offset(1, 1), spreadRadius: 5, blurRadius: 5)
            ]
        ),
      ),
    );
  }
}

