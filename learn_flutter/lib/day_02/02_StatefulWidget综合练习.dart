import 'dart:ui';

import 'package:flutter/material.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: JKHomePage(),
    );
  }
}

class JKHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "计数器",
          style: TextStyle(
              fontSize: 20,
              color: Colors.yellow
          ),
        ),
        backgroundColor: Colors.purple,
      ),
      body: JKHomeContent(),
    );
  }
}

class JKHomeContent extends StatefulWidget {
  @override
  _JKHomeContentState createState() => _JKHomeContentState();
}

class _JKHomeContentState extends State<JKHomeContent> {
  int _counter = 0;
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _getButtons(),
          Text("当前计数：$_counter")
        ],
      ),
    );
  }

  Widget _getButtons() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ElevatedButton(
            onPressed:  () {
              setState(() {
                _counter++;
              });
            },
            child: Text("+")
        ),
        SizedBox(width: 20),
        ElevatedButton(
          onPressed:  () {
            setState(() {
              _counter--;
            });
          },
          child: Text("-"),
        )
      ],
    );
  }
}

