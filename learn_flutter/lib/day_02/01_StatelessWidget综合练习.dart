import 'dart:ui';

import 'package:flutter/material.dart';

main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: JKHomePage(),
    );
  }
}

class JKHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "商品列表",
          style: TextStyle(
              fontSize: 20,
              color: Colors.yellow
          ),
        ),
        backgroundColor: Colors.purple,
      ),
      body: JKHomeContent(),
    );
  }
}

class JKHomeContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        JKHomeProductItem("Apple1", "MackBook1", "https://tva1.sinaimg.cn/large/006y8mN6gy1g72j6nk1d4j30u00k0n0j.jpg"),
        SizedBox(height: 10),
        JKHomeProductItem("Apple2", "MackBook2", "https://tva1.sinaimg.cn/large/006y8mN6gy1g72imm9u5zj30u00k0adf.jpg"),
        SizedBox(height: 10),
        JKHomeProductItem("Apple3", "MackBook3", "https://tva1.sinaimg.cn/large/006y8mN6gy1g72imqlouhj30u00k00v0.jpg"),
      ],
    );
  }
}

class JKHomeProductItem extends StatelessWidget {
  final String title;
  final String desc;
  final String imageURL;
  final style1 = TextStyle(fontSize: 22, color: Colors.brown);
  final style2 = TextStyle(fontSize: 22, color: Colors.green);
  JKHomeProductItem(this.title, this.desc, this.imageURL);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
          border: Border.all(
              width: 2,
              color: Colors.brown
          )
      ),
      child: Column(
        children: [
          Text(title, style: style1),
          SizedBox(height: 10),
          Text(desc, style: style2),
          SizedBox(height: 10),
          Image.network(imageURL)
        ],
      ),
    );
  }
}
